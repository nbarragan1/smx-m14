#4. Realitzar un programa que sol·liciti per teclat dos números, si el primer és major al segon informar la seva suma i diferència, en cas contrari informar el producte i la divisió del primer respecte al segon

num1 = int(input("Escriba un numero: "))
num2 = int(input("Escriba un numero: "))

if num1 > num2:
    print(f"La suma es {num1 + num2} y la diferencia es { num1 - num2}")
else:
    print(f"El producto es {num1 * num2} y la division es {num1 / num2}")