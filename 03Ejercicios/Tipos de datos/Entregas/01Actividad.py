#1. Mira la següent frase: “Sólo trabajo y nada de juegos hacen de Juan un niño aburrido”. Guarda cada paraula en variables separades, després mostra la frase en una sola línia.

solo = "solo "
trabajo = "trabajo "
y = "y "
nada = "nada "
de = "de "
juegos = "juegos "
hacen = "hacen "
de = "de "
Juan = "Juan "
un = "un "
ninyo = "niño "
aburrido = "aburrido"

print(solo, trabajo, y, nada, de, juegos, hacen, de, Juan, un, ninyo, aburrido)