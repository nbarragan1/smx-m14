#11. Fes un programa que demani la teva edat i mostri per pantalla si ets major d’edat. Si introdueix un número menor que 0 indiqui que és incorrecte.

edad = int(input("Introduce tu edad: "))

if edad > 17:
    print("Eres mayor de edad")
else:
    print("Eres menor de edad")

if edad < 0:
    print("Incorrecto")
