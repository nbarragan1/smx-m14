#20. Si tenim la variable A = 5, fes
# Suma en assignació amb 3
# Resta en assignació amb 2
# Multiplicació en asignació per 2.
# Divisió en asignació per 4
# Potència en asignació per 3.

A = 5
A += 3
print(A)

A = 5
A -= 2
print(A)

A = 5
A *= 2 
print(A)

A = 5
A /= 4
print(A)

A = 5
A **= 3
print(A)

