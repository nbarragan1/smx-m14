#Print la palabra reservada para imprimir.

from os import sep


print("Hola mundo")
print("Hola, mundo")
print("hola","mundo") #La fincion print acepta varios parametros

print("hola" "mundo") #Varios parametros sin sepearador

##### Ejercicio #####

print("¡Hola, Mundo!")

nombre = "Nathan"
print("Mi nombre es:", nombre)

#Si quitamos las comillas nos dara el siguiente error:

##### File "c:\Programas_Programacion\M14\smx-m14\02Ejemplos\01Ejemplos.py", line 14
#####    print(Mi nombre es:, nombre)
#####             ^^^^^^^^^
#####SyntaxError: invalid syntax. Perhaps you forgot a comma?

##Nos esta señalando que el error esta luego del print, ya que hemos quitado las comillas##



#Ahora quitare los parentesis:

#### File "c:\Programas_Programacion\M14\smx-m14\02Ejemplos\01Ejemplos.py", line 14
####    print"Mi nombre es:", nombre
####    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^
####SyntaxError: Missing parentheses in call to 'print'. Did you mean print(...)?

##Nuevamente un error de SyntaxError y nos dice que faltan parentesis en el 'print'



#Usando comillas simples, no da ningun error.



##SEPARADORES##

print("Nathan","Barragan",sep="")  #El separador es el espacio, pero aqui no es ninguno
print("Nathan","Barragan",sep="_") #Aqui el separador es _ 

##END##

print("Nathan","Barragan",sep="",end="\t")
print("Nathan", "Barragan",sep="_", end="@")
print("Mi edad es 20")

##Montar un print Fundamentos**Programacion**en...Python usando dos funciones print

print("Fundamentos", "Programacion", "en",  sep="***", end="...")
print("Python")