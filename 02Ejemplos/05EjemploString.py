#Concatenar variables de text:

from ctypes.wintypes import HLOCAL


cadena = "Hola" + "Francesc"

print(cadena)

#Ejemplo de cadena con operador concatenar
a = "Tinc"
b = 10
c = "anys"

a+str(b)+c

print(a, b, c, end="*")

#Ejemplo operador multiplicar

a = "Hola"
b = 5
print()
print(a*b, sep=" ") #El sepador no se aplica ya que es todo una linia


print(10 == 10.1)