#No funciona porque else es una palabra reservada
#else = 2
#No funciona porque false es una palabra reservada
#false = 3
#No funciona porque class es una palabra reservada
#class = 'Teorema avanzado de Zymurgy'

help('keywords')

#Las variables son int
x = 1
y = 2
print(x + y)

#Las variables son str
x = "1"
y = "2"
print(x + y)

#Ejercicio:

#a) Indica perquè són valides o no le següents variables


##VALIDO##

#Una variable a es valido porque es solo una letra y eso lo permite
#Una variable a3 es valido porque contiene una letra primero y un numero seguido lo permite
#a_b_c___95 es valido porque contiene caracteres permitidos
#_abc es valido porque tiene caracteres permitidos
#_3a es valido porque no empieza con un numero 

##INVALIDO##

#3 no es valido porque es un numero y esto no esta permitido
#3a no es valido porque empieza con un numero 
#another-name no es valido porque contiene - y eso no esta permitido
#with tampoco es valido porque es una palabra reservada
#3_a no es valido porque empieza con un numero


#b) Perquè no funciona aquesta línia de codi:
#A = 2
#print (a)

#No funciona porque de primeras pusimos la A en mayusculas y luego en el print pone una a en minuscula y eso no es lo mismo.


#No funciona porque en print no se a asignado antes ninguna variable que se llame a, se hace mas tarde pero eso no esta bien, hay que ponerlo antes
#print (a)
#a = 45

#Ejemplo float

a = 10
print(float(a))

b = 5.0
print(b)